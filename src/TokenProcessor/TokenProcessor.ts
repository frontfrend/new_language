import {
  isOperation,
  isVariable,
  isGrouping,
  isCondition,
  isNumber,
  isEndExpression,
  isNot,
} from "./CharCheckers";
import InputProcessor from "../StringProcessor/StringProcessor";
import { IToken, TokenType } from "../types";



class TokenProcessor {
  public input: InputProcessor;
  private index: number = -1;
  private current: IToken;
  constructor(input: InputProcessor) {
    this.input = input;
  }

  peek() {
    this.current = this.current || this.readNext();
    return this.current;
  }
  next() {
    const tok = this.current;
    this.current = null;
    return tok || this.readNext();
  }
  eof() {
    return this.peek() == null;
  }

  readWhile(predicate: Function) {
    let str = "";
    let isEof = this.input.eof();
    let pResult = predicate(this.input.peek());
    let nexIter = !isEof && pResult;
    while (nexIter) {
      str += this.input.peek();

      pResult = predicate(this.input.peekNext());
      if (pResult) {
        this.input.next();
      }
      isEof = this.input.eof();
      nexIter = !isEof && pResult;
    }

    return str;
  }

  readNext() {
    const ch = this.input.next();
    if (this.input.eof()) {
      return null;
    }

    if (isEndExpression(ch)) {
      return {
        type: TokenType.EndExpression,
      };
    }

    if (isNot(ch)) {
      return {
        type: TokenType.Operation,
        value: ch,
      };
    }

    if (isCondition(ch)) {
      return {
        type: TokenType.Condition,
        value: this.readWhile(isCondition),
      };
    }

    if (isOperation(ch)) {
      return {
        type: TokenType.Operation,
        value: this.readWhile(isOperation),
      };
    }

    if (isGrouping(ch)) {
      return {
        type: TokenType.Group,
        value: ch,
      };
    }

    if (isVariable(ch)) {
      return {
        type: TokenType.Variable,
        value: this.readWhile(isVariable),
      };
    }

    if (isNumber(ch)) {
      return {
        type: TokenType.Number,
        value: Number(this.readWhile(isNumber)),
      };
    }

    throw new Error(`Unexpected token ${this.input.peek()}`);
  }
}

export default TokenProcessor;
