export const isNot = (ch: string) => {
  return "!" === ch;
}

export const isOperation = (ch: string) => {
  return "=&+-/*".indexOf(ch) >= 0;
}

export const isNumber = (ch: string) => {
  return /[0-9]|\./g.test(ch)
}

export const isIf = (ch: string) => {
  return /[i,f]/g.test(ch)
}
export const isThen = (ch: string) => {
  return /[t,h,e,n]/g.test(ch)
}

export const isCondition = (ch: string) => {
  return /[i,f,t,h,e,n]/g.test(ch)
}


export const isVariable = (ch: string) => {
  return ['A', 'B', 'C', 'D', 'E', 'M', 'P', 'T', 'K', 'H', 'F'].includes(ch)
}

export const isExpressionId = (ch: string) => {
  return ['M', 'P', 'T'].includes(ch)
}

export const isEndExpression = (ch: string) => {
  return ch === ';'
}

export const isGrouping = (ch: string) => {
  return ['(', ')'].includes(ch);
}
