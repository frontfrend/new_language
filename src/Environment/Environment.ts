import Type from "./TypeChecker";
import { DataType, ExpressionType } from "../types";

class Environment {
  vars: { [key: string]: string | number | boolean } = {};
  varsType: { [key: string]: DataType } = {};
  parent: Environment;

  checkVar(name) {
    if (!(name in this.vars)) {
      throw new Error("Undefined variable " + name);
    }
  }

  checkType(newValue: any, type: DataType) {
    if (!Type.check(newValue, type)) {
      throw new Error(`${newValue} is not a ${type} type`);
    }
  }

  get(name) {
    this.checkVar(name);
    return this.vars[name];
  }
  
  set(name, value) {
    this.checkVar(name);
    this.checkType(value, this.varsType[name]);
    return (this.vars[name] = value);
  }
  def(name, value, type) {
    this.checkType(value, type);
    this.varsType[name] = type;
    return (this.vars[name] = value);
  }

  execute(exp, env) {
    switch (exp.type) {
      case ExpressionType.Number:
        return exp.value;

      case ExpressionType.Variable:
        return env.get(exp.value);

      case ExpressionType.Assign:
        return env.set(exp.left.value, this.execute(exp.right, env));

      case ExpressionType.Binary:
        return this.applyOperation(
          exp.operator,
          this.execute(exp.left, env),
          this.execute(exp.right, env)
        );

      case ExpressionType.If:
        const cond = this.execute(exp.cond, env);
        if (cond !== false) return this.execute(exp.then, env);
        return exp.else ? this.execute(exp.else, env) : false;

      case ExpressionType.Prog:
        let val = false;
        exp.prog.forEach((exp) => {
          val = this.execute(exp, env);
        });
        return val;

      case ExpressionType.Operation:
        return exp;

      default:
        throw new Error(`Unexpected type ${exp.type} at execute function`);
    }
  }

  applyOperation(op, a, b) {
    Type.checkOperation(op, a, b)

    switch (op) {
      case "+":
        return a + b;
      case "-":
        return a - b;
      case "*":
        return  a * b;
      case "/":
        return a / b;
      case "&&":
        return a !== false && b;
      case "==":
        return a === b;
      case "=":
        if (a) {
          return this.set("H", b);
        }
        return a;
      case "!":
        return !b;
    }
    throw new Error(`Can't apply operator ${op}`);
  }
}

export default Environment;
