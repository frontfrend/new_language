import { DataType } from "../types";

function isInt(n: number) {
  return n % 1 === 0;
}

function isNumber(n: any) {
  return typeof n === DataType.number;
}

function isBoolean(n: any) {
  return typeof n === DataType.boolean;
}

function throwIfNotNumber(n: any) {
  if (!isNumber(n)) {
    throw new Error(`${n} is not a number`);
  }
}

function throwIfZero(n: any, msg?: string) {
  if (n === 0) {
    throw new Error(msg || `${n} is not a number`);
  }
}

function throwIfNotBoolean(n: any, msg?: string) {
  if (!isBoolean(n)) {
    throw new Error(msg || `${n} is not a boolean`);
  }
}

function check(newValue: any, type: DataType) {
  const newValueType = typeof newValue;

  switch (type) {
    case DataType.boolean:
      return newValueType === type;
    case DataType.string:
      return newValueType === type;
    case DataType.int:
      if (newValueType === DataType.number && !isNaN(newValue)) {
        if (isInt(newValue)) {
          return true;
        }
      }
      return false;
    case DataType.float:
      if (newValueType === DataType.number && !isNaN(newValue)) {
        return true;
      }
      return false;
  }
}

function checkOperation(operation: string, a: any, b: any) {
  switch (operation) {
    case "+":
    case "-":
    case "*":
      throwIfNotNumber(a);
      throwIfNotNumber(b);
      return;
    case "/":
      throwIfNotNumber(a);
      throwIfNotNumber(b);
      throwIfZero(b, "Divide by zero");
      return;
    case "&&":
      throwIfNotBoolean(a);
      throwIfNotBoolean(b);
      return;
    case "=":
      if (a) {
        return this.set("H", b);
      }
      return a;
    case "!":
      throwIfNotBoolean(b);
      return !b;
  }
}

const Type = {
  check,
  checkOperation,
};

export default Type;
