import TokenProcessor from "./TokenProcessor/TokenProcessor";
import StringProcessor from "./StringProcessor/StringProcessor";
import Parser from "./Parser/Parser";
import Environment from "./Environment/Environment";
import { DataType } from "./types";

// const A = true;
// const B = true;
// const C = false;
// const D = 2.8;
// const E = 4;
// const F = 6;

// const input = `
// A && B && !C => (H = M)
// A && B && C => H = P
// !A && B && C => H = T
// H = M => K = D + (D * E / 10)
// H = P => K = D + (D * (E - F) / 25.5)
// H = T => K = D - (D * F / 30)
// H = P => K = 2 * D + (D * E / 100)
// A && B && C => H = M
// A && B && !C => H = T
// H = T => K = F + D + (D * E / 100)
// `;

// const stringProcessor = new StringProcessor(input);
// const tokenProcessor = new TokenProcessor(stringProcessor);
// const parser = new Parser(tokenProcessor);
// const ast = parser.getASTTree();
// console.log(ast);
// console.log(JSON.stringify(ast));



// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());
// console.log(tokenProcessor.readNext());

const getCheckboxValue = (id) => {
  const cbox = document.getElementById(id) as HTMLInputElement;
  return cbox.checked
}

const getNumberValue = (id) => {
  const input = document.getElementById(id) as HTMLInputElement;
   
  return Number(input.value)
}
(window as any).run = () => {
  const sourceCode = document.getElementById('sourceCode') as HTMLInputElement;
  const output = document.getElementById('output') as HTMLInputElement;


  
  const A = getCheckboxValue('inputA');
  const B = getCheckboxValue('inputB');
  const C = getCheckboxValue('inputC');
  const D = getNumberValue('inputD');
  const E = getNumberValue('inputE');
  const F = getNumberValue('inputF');
  try {
    const stringProcessor = new StringProcessor(sourceCode.value);
    const tokenProcessor = new TokenProcessor(stringProcessor);
    const parser = new Parser(tokenProcessor);
    const ast = parser.getASTTree();
  
    const environment = new Environment();
    environment.def("A", A, DataType.boolean);
    environment.def("B", B, DataType.boolean);
    environment.def("C", C, DataType.boolean);
    environment.def("D", D, DataType.float);
    environment.def("E", E, DataType.int);
    environment.def("F", F, DataType.int);
    environment.def("M", "M", DataType.string);
    environment.def("P", "P", DataType.string);
    environment.def("T", "T", DataType.string);
    environment.def("K", 0, "float");
    environment.def("H", "", DataType.string);
    environment.execute(ast, environment);
    output.value = `K = ${environment.vars.K}`
  }catch(e) {
    output.value = e.message
  }
  
}