import TokenProcessor from "../TokenProcessor/TokenProcessor";
import { TokenType } from "../types";

enum Priority {
  "=" = 2,
  "=>" = 3,
  "==" = 80,
  "&&" = 80,
  "||" = 80,
  "!" = 85,
  "+" = 90,
  "-" = 90,
  "*" = 100,
  "/" = 100,
}

class Parser {
  tp: TokenProcessor;
  constructor(tokenProcessor: TokenProcessor) {
    this.tp = tokenProcessor;
  }

  isCondition() {
    const tok = this.tp.peek();
    return tok && tok.type == TokenType.Condition && tok.value == "if" && tok;
  }

  skipGrouping(ch) {
    if (this.isGrouping(ch)) {
      this.tp.next();
    } else {
      throw new Error(this.tp.peek().type);
    }
  }

  isGrouping(ch) {
    const token = this.tp.peek();
    return (
      token &&
      token.type == TokenType.Group &&
      (!ch || token.value == ch) &&
      token
    );
  }

  isOperation(op?: string) {
    const tok = this.tp.peek();
    return (
      tok && tok.type == TokenType.Operation && (!op || tok.value == op) && tok
    );
  }

  parseIf() {
    const ifToken = this.tp.peek();
    if (ifToken.value !== "if") {
      throw new Error("Expected begin of condition but get:" + ifToken);
    }
    this.tp.next();
    const cond = this.parseExpression();
    const thenToken = this.tp.peek();
    if (thenToken.value !== "then") {
      throw new Error("Expected end of condition but get:" + thenToken);
    }
    this.tp.next();
    const then = this.parseExpression();
    const ret = {
      type: "if",
      cond: cond,
      then: then,
    };

    return ret;
  }

  maybeBinary(left, my_prec) {
    const tok = this.isOperation();
    if (tok) {
      const his_prec = Priority[tok.value];
      if (his_prec > my_prec) {
        this.tp.next();

        return this.maybeBinary(
          {
            type: tok.value == "=" ? "assign" : "binary",
            operator: tok.value,
            left: left,
            right: this.maybeBinary(this.parseAtom(), his_prec),
          },
          my_prec
        );
      }
    }
    return left;
  }

  parseAtom() {
    if (this.isGrouping("(")) {
      this.tp.next();
      const exp = this.parseExpression();
      this.skipGrouping(")");
      return exp;
    }
    if (this.isCondition()) return this.parseIf();
    if (this.isOperation()) {
      return this.tp.peek();
    }

    const tok = this.tp.next();
    if (tok.type == TokenType.Variable || tok.type == TokenType.Number)
      return tok;

    throw new Error(this.tp.peek().type);
  }

  parseExpression() {
    return this.maybeBinary(this.parseAtom(), 0);
  }

  getASTTree() {
    const prog = [];
    while (!this.tp.eof()) {
      prog.push(this.parseExpression());
      if (!this.tp.eof()) {
        this.tp.next();
      }
    }
    return { type: "prog", prog: prog };
  }
}

export default Parser;
