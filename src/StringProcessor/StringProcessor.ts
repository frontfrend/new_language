class StringProcessor {
  private position: number = -1;
  private input: string;

  constructor(input: string) {
    const withoutWhitespace = input.replace(/ /g, "");
    const replacedThen = withoutWhitespace.replace(/=>/g, "then");
    const replacedIf = replacedThen.replace(/\n/g, ";if");

    const droppedImptyLines = replacedIf
      .split(";")
      .filter((row) => !["", "if"].includes(row))
      .map((row) => {
        if (row.slice(0, 2) !== "if") {
          return `if${row}`;
        }

        return row;
      });
    const withEqual = droppedImptyLines.join(";").replace(/ifH=/g, "ifH==");
    this.input = withEqual; 
  }

  peek() {
    return this.input[this.position];
  }

  next() {
    const newIndex = ++this.position;

    const ch = this.input[newIndex];
    return ch;
  }

  peekNext() {
    return this.input[this.position + 1];
  }

  eof() {
    return this.input.length <= this.position;
  }
}

export default StringProcessor;
