export enum DataType {
  "boolean" = "boolean", 
  "float" = "float", 
  "int" = "int", 
  "string" = "string", 
  "number" = "number", 
}

export enum TokenType {
  Operation = 'op',
  Condition = 'condition',
  Group = 'group',
  Variable = 'var',
  Number = 'number',
  EndExpression = 'end-expression',
};

export enum ExpressionType {
  "Number" = 'number',
  Operation = 'op',
  Binary = 'binary',
  Variable = 'var', 
  Assign = 'assign',
  If = 'if',
  Prog = 'prog',
};

export interface IToken {
  type: TokenType;
  [key:string]: any;
}