# Down Language

Quick Start
  1. "git clone https://gitlab.com/frontfrend/new_language"
  2. go to the root of the cloned repository
  3. run "yarn" or "npm i"
  4. run "yarn dev" or "npm run dev"
  5. then open [http://localhost:1234/](http://localhost:1234/)

# About

# RU 
Первая идея заключалась в преобразовании строки и в последствии вызов eval. Это было бы самое быстрое решение, но архитектурой там не похло.
Вторая идея заключалась в написании своего мини языка. Начав копать по этой теме наткнулся на Bison, llvm. Осознав что жизнь коротка как и дедлайны и бороды у меня нет продолжил поиски и нашел блог одного разработчика уже писавшего подобный велосипед. После прочтения взяв всю смелось в руки начал лепить свой велосипед и вот что получилось.

### 1-й уровеь - это StringProcessor. Он делает 2 вещи
 - Преобразовывает код программы из
      ```js
        A && B && !C => H = M
        H = M => K = D + (D * E / 10)
      ```
    в
      ```js
        if A && B && !C then H = M
        if H == M then K = D + (D * E / 10)
      ```
    что заметно упростило разбор выражения 
   
  - Предоставляет методы для перемещения по строке


### 2-й уровень - это TokenProcessor
Если прерыдущий слой работал со строкой, то этот разбивает строку на части кода (токены) в процессе пользования методами.

### 3-й уровень - это Parser

Он определяет приоритет операторов и на основе этого создает ast (Абстрактное синтаксическое дерево). В большинстве узел дерева имеет следующую структуру:
```js
Node: {
  operator: '+' // +, -, ...
  left: Node,
  right: Node,
}
```

### 4-й уровень - это Environment
Этот уровень проходит по дереву и по сути исполняет программу. Так же тут находятся проверки типов 

# ENG

The first idea was to use eval and sourceCode.replace('A', 'value of A from input') but there is no architecture and it's not looks like an extendable or maybe easy extendable solution.
The second idea was to create mini language. I found a blog of young bicycle developer and did this:

### 1st level - is StringProcessor. It does 2 things
 - transform source code from
      ```js
        A && B && !C => H = M
        H = M => K = D + (D * E / 10)
      ```
    into
      ```js
        if A && B && !C then H = M
        if H == M then K = D + (D * E / 10)
      ```
    That makes parsing easy 
   
  - Add methods for navigation by a string


### 2nd level - is TokenProcessor
It separate string into tokens. Token - is the simplest program part (A - variable, + - operator, if - condition, 21 - number, etc)

### 3rd level - is Parser
It define priority of operators and makes ast (Abstract syntax tree). In most cases a node of the tree has the next structure:
```js
Node: {
  operator: '+' // +, -, ...
  left: Node,
  right: Node,
}
```

### 4th level - is Environment
- does all operations between nodes
- checks types at define variable set new value and before operation